package club.ximeng.blog.framework.common.util.metadata;

/**
 * @author liu xm
 * @date 2022-02-10 14:11
 */
public class GetStringLength {

    /**
     * 获取字符串长度 😑😑😑😑😑
     *
     * @param str .
     * @return 字符串长度
     */
    public static int getLength(String str) {
        int length = 0;
        for (int i = 0; i < str.length(); i++) {
            length++;
        }
        return length;
    }
}
